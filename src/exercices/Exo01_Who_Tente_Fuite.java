package exercices;

import entites.DocWho;
import entites.Ood;

public class Exo01_Who_Tente_Fuite {

    public static void main(String[] args) {

        DocWho doc       = new DocWho();
        Ood       unOod   = new Ood();
        
        //<editor-fold defaultstate="collapsed" desc="A compléter">
        
        while(doc.estVivant()&&doc.naPasFuit())
        {
        doc.afficher();
        doc.tenteDeFuir();
        if(doc.aFuit())
        {
                   
                   System.out.println("Doc Who a réussi à fuir !");    
        }          
        else
        {
        unOod.attaque(doc);
        if(doc.estMort())
                {
                doc.afficher();
                System.out.println("Doc Who est mort");
                }
            }
        }
        
        
        //</editor-fold>
    }
}


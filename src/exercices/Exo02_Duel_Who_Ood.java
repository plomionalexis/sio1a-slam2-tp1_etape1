package exercices;
import entites.DocWho;
import entites.Ood;
import static  utilitaires.Hasard.*;

public class Exo02_Duel_Who_Ood {

    public static void main(String[] args) {

        DocWho doc       = new DocWho();
        Ood       leOod   = new Ood();
         int tirage;
        boolean fin = false;
        //<editor-fold defaultstate="collapsed" desc="A COMPLETER">
                 System.out.println("Duel Docteur Who vs Ood");
            
           
            System.out.println();     
            
             while(fin == false)  {
                tirage = aleat(1,3);
                if(tirage == 1) { 
                    doc.attaque(leOod); 
                    System.out.printf("L'ood a "+leOod.getNbVies()+" points de vie, Doc Who a "+doc.getNbVies()+" points de vie et il attaque\n"); 
                }
                if(tirage == 2) {
                    doc.seDefend();
                    System.out.printf("L'ood a "+leOod.getNbVies()+" points de vie, Doc Who a "+doc.getNbVies()+" points de vie et il se défend\n");
                }
                if(tirage == 3) {
                    doc.tenteDeFuir(); System.out.printf("L'ood a "+leOod.getNbVies()+" points de vie, Doc Who a "+doc.getNbVies()+" points de vie et il tente de fuir\n");
                    leOod.attaque(doc);
                }
                
                if(doc.estVivant() && leOod.estMort()) {
                    System.out.println("Le Docteur Who a gagné");
                    fin = true;
                } 
                if(doc.estMort()){
                    System.out.println("L'ood a gagné");
                    fin = true;
                }
                if(doc.aFuit()){
                    System.out.println("Le Docteur Who a gagné");
                    fin = true;
                }
                
            }    
        }
        
        
        
        //</editor-fold>
    }





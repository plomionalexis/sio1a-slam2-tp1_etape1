package entites;

import static utilitaires.Hasard.aleat;

public class DocWho {

     private final        int       FORCE_MIN= 0, FORCE_MAX=8;  
    
     private boolean   fuiteReussie          =   false;
     private boolean   defenseReussie     =   false;
     private int           nbVies                  =   12;
  
     public void          attaque(Ood unOod, int forceAttaque )  {  unOod.subitAttaque(forceAttaque); }
  
     public void          attaque(Ood unOod )                            { attaque(unOod,aleat(FORCE_MIN, FORCE_MAX)); }
   
     public void          tue(Ood unOod)                                   { attaque(unOod, unOod.getNbVies());}
 
     public void          subitAttaque( int forceAttaque)            {
      
       if  (defenseReussie )     defenseReussie=false;
       else                              nbVies = forceAttaque<nbVies ? nbVies-forceAttaque : 0 ;                                        
    }
   
     public void          seDefend()       {  defenseReussie = aleat(0, 100)>60; }
 
     public void          tenteDeFuir( )   {  fuiteReussie     = aleat(0, 1000)>990 ; } 
  
     public boolean     estVivant()       { return nbVies>0; } 
  
     public boolean     estMort()         { return ! estVivant() ;  }

     public boolean     aFuit()             { return fuiteReussie;}

     public boolean     naPasFuit()      {  return !fuiteReussie; }
   
     public void           afficher()        {
           
           System.out.printf("Doc Who Force vitale: %2d Force de frappe min:%2d  max:%2d\n",
                            nbVies,
                            FORCE_MIN,
                            FORCE_MAX);
     }

    public int              getNbVies()     { return nbVies; }

}



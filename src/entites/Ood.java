package entites;
import static utilitaires.Hasard.aleat;

public class Ood {
    
     private final      int    FORCE_MIN=0, FORCE_MAX=aleat(1,2);  
    
     private int          nbVies = aleat(15, 20);
   
     public void         attaque(DocWho doc )               { doc.subitAttaque( aleat(FORCE_MIN, FORCE_MAX) ); }
     public void         subitAttaque(int forceAttaque)  { nbVies= forceAttaque<nbVies? nbVies-forceAttaque:0 ; }
   
     public boolean    estVivant()    { return nbVies>0; } 
     public boolean    estMort()      { return nbVies==0;}
     
     public void afficher()               {
           
           System.out.printf("Ood Force vitale: %d Force de frappe min:%2d  max:%2d\n",nbVies,FORCE_MIN,FORCE_MAX);
     }

    public int getNbVies() {
        return nbVies;
    }
    
    } 
 





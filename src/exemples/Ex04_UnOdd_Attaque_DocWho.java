package exemples;

import entites.DocWho;
import entites.Ood;

public class Ex04_UnOdd_Attaque_DocWho {

    public static void main(String[] args) {

        DocWho doc       = new DocWho();
        Ood       unOod   = new Ood();
        
       doc.afficher();
        
       unOod.attaque(doc);
        
       doc.afficher();
    }
}
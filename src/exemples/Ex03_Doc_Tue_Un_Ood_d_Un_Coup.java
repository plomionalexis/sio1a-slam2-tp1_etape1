
package exemples;

import entites.DocWho;
import entites.Ood;

public class Ex03_Doc_Tue_Un_Ood_d_Un_Coup {

    public static void main(String[] args) {

        DocWho doc = new DocWho();
        
        Ood unOod=new Ood();
        
       System.out.println("\nL'Ood créé a "
                            + unOod.getNbVies()+ 
                            " points de vies");
        
       doc.tue(unOod); 
        
       System.out.println("\nL'Ood a maintenant  "
                            + unOod.getNbVies()+ 
                            " points de vies");
       System.out.println();
     
    }
}

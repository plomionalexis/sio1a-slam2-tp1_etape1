
package exemples;

public class Ex00_Tirages_AleatoiresV1 {

    public static void main(String[] args) {
   
        for (int i=1; i<=10;i++){
        
            System.out.printf( "Tirage n° %2d : %4d \n",
                              i,
                              utilitaires.Hasard.aleat(1,1000) 
                              );
        
        }        
    }
}

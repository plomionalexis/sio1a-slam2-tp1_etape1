package exemples;

import entites.DocWho;
import entites.Ood;

public class Ex02_Doc_Attaque_Un_Ood_Aleat {

    public static void main(String[] args) {

        DocWho doc      = new DocWho();
        Ood        unOod = new Ood();
 
        doc.afficher();
        
       System.out.println("\nL'Ood créé a "
                            + unOod.getNbVies()+ 
                            " points de vie");
        
       doc.attaque(unOod); 
        
       System.out.println("\nL'Ood a maintenant "
                           + unOod.getNbVies()+ 
                            " points vies\n");
    
    }
}

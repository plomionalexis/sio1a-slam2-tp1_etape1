package exemples;

import entites.DocWho;
import entites.Ood;

public class Ex01_Doc_Attaque_Un_Ood {

    public static void main(String[] args) {

        DocWho doc  = new DocWho();
        Ood unOod    = new Ood();
       
        doc.afficher();
        unOod.afficher();
 
        doc.attaque(unOod, 4);
        System.out.println("\nL'Ood  a maintenant "
                            + unOod.getNbVies()+ 
                            " points de vies\n");
    }
}

package exemples;

import static  utilitaires.Hasard.aleat;

public class Ex00_Tirages_AleatoiresV2 {

    public static void main(String[] args) {
   
        for (int i=1; i<=10;i++){
        
            System.out.printf( "Tirage n° %2d : %4d \n",
                                i,
                                aleat(1,1000)
                                );        
        }        
    }
}

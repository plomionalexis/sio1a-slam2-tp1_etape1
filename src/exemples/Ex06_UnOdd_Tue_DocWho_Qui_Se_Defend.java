package exemples;

import entites.DocWho;
import entites.Ood;

public class Ex06_UnOdd_Tue_DocWho_Qui_Se_Defend {

    public static void main(String[] args) {

        DocWho doc       = new DocWho();
        Ood       unOod   = new Ood();
        
        doc.afficher();
        unOod.afficher();
        System.out.println();
        
         while( doc.estVivant())  {
        
             doc.seDefend();
             unOod.attaque(doc);
             doc.afficher();
         }    
    }
}

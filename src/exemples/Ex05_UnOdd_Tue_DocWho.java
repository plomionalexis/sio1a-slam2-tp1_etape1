
package exemples;

import entites.DocWho;
import entites.Ood;

public class Ex05_UnOdd_Tue_DocWho {

    public static void main(String[] args) {

        DocWho doc       = new DocWho();
        Ood       unOod   = new Ood();
        
        doc.afficher();
        
         while( doc.estVivant() )  {
        
             unOod.attaque(doc);
             doc.afficher();
         }            
    }
}
